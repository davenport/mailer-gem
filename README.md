![mail monkey logo](http://media.davenport.edu/sites/default/files/monkey_logo.png)

# Overview

The Mail Monkey (aka the Mailer Gem) is a ruby gem for interfacing with DU's [mail queue web service](https://bitbucket.org/davenport/mail-queue). By adding Mail Monkey to your ruby app, you no longer have to worry about sending emails directly from your app.  Mail Monkey and the mail web queue service take care of it.  Mail Monkey is bananas for email!

# Installation

Mail Monkey can be installed from the DU gem server located at [gem server URL].  You need to declare the gem in your Gemfile as normal:

	:::ruby
		gem 'du-mailer'

And also include the du gem server in your source declarations:
	
	:::ruby
		source 'du_gem_server_url'

This should go above the rubygems source declaration.

# Usage

The first step to simian email nirvana is to visit the Mail Queue app ([prod](http://mailer.davenport.edu/) or [staging](http://mailer-staging.davenport.edu/)) to register your application and generate an API token.

A simple example (assumes some sort of config file):

	:::ruby
		require 'du-mailer'
		include DuMailer
		
		# Instantiate a new mail queue
		mailer = MailQueue.new(settings.mailer_url, settings.mailer_token)
		
		# Create n email records
		mailer.create_record(	'joe@example.com',			# To	
								'noreply@davenport.edu',   	# From
								'Etiam Inceptos Egestas', 	# Subject
								'Nullam Etiam Malesuada',	# Body
								Time.now)					# Send on or before
		
		mailer.create_record(	'jane@example.com',			# To	
								'noreply@davenport.edu',   	# From
								'Etiam Inceptos Egestas', 	# Subject
								'Nullam Etiam Malesuada',	# Body
								nil)						# Setting nil will cause the email to never be sent, for development purposes.
								
		# Submit emails to web service
		mailer.send_payload
		
It is most efficient to fill the MailQueue object with all the emails you want to send then submit it's payload, as opposed to multiple send_payload calls with smaller batches of emails.  The gem and it's supporting web service have been stress tested with a 10,000 email load which they handle no problems at all.