require 'spec_helper.rb'
include Mailer

describe 'Mailer Classes' do
	
	context 'Mail Queue' do

		it "should return a Mail Queue object" do
			MailQueue.new("", "").should be_kind_of(MailQueue)
		end
		
		it "should create a Mail Record" do
			mq = MailQueue.new("", "")
			mq.create_record("test", "test", "test", "test", nil)
		end
		
		it "should respond to POST request with JSON object" do
			mq = MailQueue.new("https://mailer-staging.davenport.edu/mail/create", "123456789")
			mq.create_record("test", "test", "test", "test", Time.now)
			stub_request(:post, "https://X:123456789@mailer-staging.davenport.edu/mail/create").
			         with(:headers => {'Accept'=>'*/*', 'User-Agent'=>'Ruby'}).
			         to_return(:status => 200, :body => "", :headers => {})
			
			mq.send_payload
			
			a_request(:post, "https://X:123456789@mailer-staging.davenport.edu/mail/create").with { |req| req.body.should include("to", "from", "subject", "body", "send_on_or_after") }.should have_been_made
		end
	
	end
	
	context 'Mail Record' do
		
		it "should create an Mail Record object" do
			MailRecord.new({}).should be_kind_of(MailRecord)
		end
		
		it "should return object data as a hash" do
			mr = MailRecord.new({:to => "test_user@example.com", :from => "test", :subject => "test", :body => "test", :send_on_or_after => Time.now})
			mr.to_hash.should be_kind_of(Hash)
			mr.to_hash.should include("to", "from", "subject", "body", "send_on_or_after")
		end
		
	end

end