require 'json'
require 'net/http'

module DuMailer
	class MailQueue
		attr_accessor :payload, :service_url, :token
		
		def initialize(service_url, token)
			@service_url = service_url
			@token = token
			@payload = {}
			payload['mail_requests'] = []
		end
		
		def create_record(to, from, subject, body, send_on_or_after)
			payload['mail_requests'] << MailRecord.new( { :to => to, 
																										:from => from, 
																										:subject => subject, 
																										:body => body, 
																										:send_on_or_after => send_on_or_after } ).to_hash
		end
		
		def send_payload

			encoded_url = URI.encode(service_url)
			url = URI.parse(encoded_url)
			full_path = (url.query.nil?) ? url.path : "#{url.path}?#{url.query}"
			req = Net::HTTP::Post.new(full_path)
			req.basic_auth "X", token
			req.add_field "Content-Type", "application/json"
			req.body = payload.to_json
			http = Net::HTTP::new(url.host, url.port)
			if url.scheme == "https"
				http.use_ssl = true
				# http.verify_mode = OpenSSL::SSL::VERIFY_NONE
			end
			
			http.request(req).body
		end
	end
	
	class MailRecord
		attr_accessor :to, :from, :subject, :body, :send_on_or_after
		
		def initialize(data)
			data.each do |k,v|
				send("#{k.downcase}=",v)
			end
		end
		
		def to_hash
			{
      	'to'=>self.to,
				'from'=>self.from,
				'subject'=>self.subject,
				'body'=>self.body,
				'send_on_or_after'=>self.send_on_or_after
    	}
		end
			
	end
end