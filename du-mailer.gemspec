Gem::Specification.new do |s|
  s.name        = 'du-mailer'
  s.version     = '1.0.0'
  s.date        = '2013-10-28'
  s.summary     = "A gem to interface with DU's mail queue service"
  s.description = "A gem to interface with DU's mail queue service"
  s.authors     = ["Josh Isaak"]
  s.email       = 'jisaak@davenport.edu'
  s.files       = ["lib/du-mailer.rb"]
  s.homepage    =
    'http://www.davenport.edu'
  s.license     = 'MIT'
	s.add_runtime_dependency 'json'
	s.test_files = ['spec/mailer_spec.rb']
end